---
layout: page
title: About
weight: 1
---
## Mission Statement
Key Club is an international student-led organization which provides its members with opportunities to provide service, build character and develop leadership.

## Vision
We are caring and competent servant leaders transforming communities worldwide.

## Core values
The core values of Key Club International are leadership, character building, caring and inclusiveness.

## Pledge
I pledge, on my honor, to uphold the Objects of Key Club International; to build my home, school and community; to serve my nation and God; and combat all forces which tend to undermine these institutions.

## Motto
Caring–Our Way of Life

## Objects
* To develop initiative and leadership.
* To provide experience in living and working together.
* To serve the school and community.
* To cooperate with the school principal.
* To prepare for useful citizenship.
* To accept and promote the following ideals:
  * To give primacy to the human and spiritual, rather than to the material values of life.
  * To encourage the daily living of the Golden Rule in all human relationships.
  * To promote the adoption and application of higher standards in scholarship, sportsmanship and social contacts.
  * To develop, by precept and example, a more intelligent, aggressive, and serviceable citizenship.
  * To provide a practical means to form enduring friendships, to render unselfish service and to build better communities.
  * To cooperate in creating and maintaining that sound public opinion and high idealism which makes possible the increase of righteousness, justice, patriotism and good will.
