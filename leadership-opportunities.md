---
layout: page
title: Leadership Opportunities
weight: 7
---
<h1>2016-2017 T-Shirt Designs</h1>
<p>Call for WM Key Club 2016-2017 T-Shirt Design Entries!! DEADLINE HAS BEEN EXTENDED- NEW DEADLINE IS SEPTEMBER 23RD AT @2PM</p>
<a href="/public/leadership/shirts-2017.zip">Click here for some helpful resources</a>.
<iframe src="http://docs.google.com/gview?url=http://wmkeyclub.com/public/leadership/t-shirt.pdf&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe>

<hr>

<h1>2016-2017 Key Club Chairperson Positions</h1>
<p>2016-2017 Key Club Chairperson Position Application DEADLINE IS SEPTEMBER 30 AT @2PM</p>
<iframe src="http://docs.google.com/gview?url=http://wmkeyclub.com/public/leadership/pos.pdf&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe>
<iframe src="http://docs.google.com/gview?url=http://wmkeyclub.com/public/leadership/desc.pdf&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe>
