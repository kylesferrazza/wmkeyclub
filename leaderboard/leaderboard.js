var points = []

function loadBoards() {
  var queryStringLB = "SELECT%20A,B,C,D,E";
  var sheet = "1642323900";
  var docsCodeLB = "1qY_g7c9mKXX-V3rdsiy6eoqCNb7nqWKAabPwfss2TSc";
  var queryURLLB = "https://docs.google.com/spreadsheets/d/" + docsCodeLB + "/gviz/tq?gid=" + sheet + "&headers=2&tq=" + queryStringLB;
  var queryLB = new google.visualization.Query(queryURLLB);
  queryLB.send(handleQueryResponse);
}

function inval() {
  document.getElementById("errorLB").innerHTML = "Error in generating leaderboard.";
}

function handleQueryResponse(response) {
  if (response.isError()) {
    inval();
    return;
  }

  try {
    var data = response.getDataTable();
    for (c=0; c<data.getNumberOfRows(); c++) {
      var stu = []
      for (r=0; r<data.getNumberOfColumns(); r++) {
        stu.push(data.getValue(c, r));
      }
      points.push(stu);
    }
    points.sort(function(a,b) { return a[4]-b[4] });
    points.reverse();

    addStudents(0, setupBoard(0));
    //addStudents(9, setupBoard(9));
    addStudents(10, setupBoard(10));
    addStudents(11, setupBoard(11));
    addStudents(12, setupBoard(12));

  } catch (err) {
    console.error(err);
    inval();
    return;
  }
}

function setupBoard(grade) {
  var result = []
  for (x = 0; x < points.length; x++) {
    var one = (points[x][3] == grade) && (points[x][0] != "YES");
    var two = (grade == 0 && (points[x][0] != "YES"));
    if (one || two) {
      result.push(points[x]);
    }
    if (result.length >= 5) break;
  }
 console.log(result);
  return result;
}

function addStudents(grade, set) {
  console.log(set);
  var str = document.getElementById(grade.toString()).innerHTML;
  for (x = 0; x<set.length; x++) {
    var msg = "<tr><td>" + (x+1) + "</td><td>" + (set[x][1] + " " + set[x][2]) + "</td><td>" + set[x][4] + "</td>\n";
    console.log("MSG: " + msg);
    str += msg;
    console.log("STR: " + str);
  }
  document.getElementById(grade.toString()).innerHTML = str;
}

loadBoards();
