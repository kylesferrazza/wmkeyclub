---
layout: page
title: Points Leaderboard
weight: 4.5
---
<div id="errorLB" style="color:red;"></div>

<style>
table {
  text-align:center;
}
</style>

<table id="0">
  <h2>Combined Grades</h2>
  <tr style="font-weight: bold;">
    <td>#</td>
    <td>NAME</td>
    <td>POINTS</td>
  </tr>
</table>
<hr style="border-color:#1e427a;">
<!--<table id="9">
  <h2>Ninth Grade</h2>
  <tr style="font-weight: bold;">
    <td>#</td>
    <td>NAME</td>
    <td>POINTS</td>
  </tr>
</table>-->
<table id="10">
  <h2>Tenth Grade</h2>
  <tr style="font-weight: bold;">
    <td>#</td>
    <td>NAME</td>
    <td>POINTS</td>
  </tr>
</table>
<table id="11">
  <h2>Eleventh Grade</h2>
  <tr style="font-weight: bold;">
    <td>#</td>
    <td>NAME</td>
    <td>POINTS</td>
  </tr>
</table>
<table id="12">
  <h2>Twelfth Grade</h2>
  <tr style="font-weight: bold;">
    <td>#</td>
    <td>NAME</td>
    <td>POINTS</td>
  </tr>
</table>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">google.load("visualization", "1");</script>
<script type="text/javascript" src="leaderboard.js"></script>
