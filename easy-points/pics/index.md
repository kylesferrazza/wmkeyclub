---
layout: easy-points
weight: 2
title: Take pictures/videos at Key Club events
points: (1 Point per photo/video)
---
- Post on Social Media / Email to [board@wmkeyclub.com](mailto:board@wmkeyclub.com)
- Pictures must be appropriate
- **NOTE**- this does not mean, take 300 photos, earn 300 points!! There is a limit to the number of points you may earn for this. Just send us photos/videos anyways. 
- You never know... maybe you’ll end up on the cover of the WM Key Club website or maybe even Key Club International’s website ;)
