---
layout: page
title: "Easy Points Giveaways"
weight: 3.5
---
<hr>
<div class="posts">
  {% assign pages_list = site.pages | sort:"weight" %}
  {% for post in pages_list %}
    {% if post.layout == "easy-points" %}
  <div class="post" style="margin-bottom:0px;">
    <h2 class="post-title">
      {{post.title}}
    </h2>
    <span class="post-date">{{post.points}}</span>
        {{ post.content }}
    <hr>
  </div>
    {% endif %}
  {% endfor %}
</div>
