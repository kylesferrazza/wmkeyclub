---
layout: easy-points
weight: 1
title: Learn and Memorize the Key Club Pledge
points: 5 Points
---
If you lead the Key Club pledge for the entire club at a Wednesday club meeting, you can receive 5 points.
