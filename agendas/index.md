---
layout: page
title: "Agendas"
weight: 5
---
<div class="posts" style="width:100%;height:100px;">
  {% assign files_list = site.static_files %}
  {% for file in files_list reversed %}
    {% if file.path contains "agendas/pdf/" %}
  <a href="http://docs.google.com/gview?url=http://wmkeyclub.com/{{file.path}}&embedded=true" style="width:100%; height:500px;" frameborder="0">{{ file.path | remove: ".pdf" | date_to_string}}</a>
  <br>
    {% endif %}
  {% endfor %}
</div>
