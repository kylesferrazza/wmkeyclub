---
layout: default
title: Home
weight: 0
---
<div class="page">
  <h1 class="page-title">WM Key Club</h1>

  Want to learn more about Key Club internationally? <a href="http://keyclub.org/" target="_blank">Click here!</a>
  <br><br>
  <style>
  .responsiveCal {
    position: relative;
    padding-bottom: 75%;
    height: 0;
    overflow: hidden;
  }
  .responsiveCal iframe {
    position: absolute;
    top:0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  </style>
<!--  <p align="center" style="width:100%;position:relative;text-align:center;">
<strong>DKC INFO</strong><br>
The application can be filled out online <a href="http://dkc-app.nydkc.org">HERE</a><br>
If you have any questions or need a running log of all the events you volunteered at or need a recommendation or need any further help with the application, email us at <a href="mailto:board@wmkeyclub.com">board@wmkeyclub.com</a>!!<br>
</p>
  <iframe src="http://docs.google.com/gview?url=http://wmkeyclub.com/public/dkc-help.pdf&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe><br>-->
  <a href="/public/poster.png" target="_blank">Kiwanis Concert Poster - price has been lowered to $20 for key club members and their families!</a><br><br>
  Our event calendar:<br>
  <div class="responsiveCal">
    <iframe src="https://calendar.google.com/calendar/embed?src=bveck7ukc8s0hdlubp7qiq9b1g%40group.calendar.google.com&ctz=America/New_York&mode=MONTH" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
  </div>
</div>
