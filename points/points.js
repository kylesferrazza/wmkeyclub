var $max = 70;

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

function loadPoints() {
    var pname = document.getElementById("pin").value.split(" ");
    if (pname.length > 3) {
        document.getElementById("error").innerHTML = "Please enter your full name.";
        return;
    }
    pname[0] = pname[0].toProperCase();
    pname[1] = pname[1].toProperCase();
    if (pname[2] != "" && pname[2] != undefined) {
      pname[1] += " " + pname[2].toProperCase();
    }
    var queryString = "select%20E%2CB%2CC%20where%20B%3D%27"+pname[0]+"%27%20and%20C%3D%27"+pname[1]+"%27";
    var docsCode = "1qY_g7c9mKXX-V3rdsiy6eoqCNb7nqWKAabPwfss2TSc";
    var queryURL = "https://docs.google.com/spreadsheets/d/" + docsCode + "/gviz/tq?gid=1642323900&headers=2&tq=" + queryString;
    var query = new google.visualization.Query(queryURL);
    query.send(handleQueryResponse);
}

function inval() {
    document.getElementById("error").innerHTML = "\'" + document.getElementById("pin").value + "\' not found.";
}

function handleQueryResponse(response) {
    if (response.isError()) {
        inval();
        return;
    }

    var data, pts, name;
    console.log(response.getDataTable());
    try {
        data = response.getDataTable();
        pts = data.getValue(0, 0);
        name = data.getValue(0, 1) + " " + data.getValue(0,2);
    } catch (err) {
        inval();
        return;
    }
    if (name == null || name == "") {
        inval();
        return;
    }
    
    console.log("no error");

    ptsText = "points";
    if (pts == 1) ptsText = "point";
    document.getElementById("points").innerHTML = pts + " " + ptsText;
    
    document.getElementById("prog").value = pts;
    document.getElementById("prog").max = $max;
    document.getElementById("max").innerHTML = $max;
    
    document.getElementById("percent").innerHTML = ((Math.round((pts / $max)*1000)) / 10) + "%";

    document.getElementById("name").innerHTML = "Hi " + name;
    document.getElementById("instructions").style.display = "none";
    document.getElementById("pointsText").style.display = "block";
}
