---
layout: page
title: "Check Your Points"
weight: 4
---

<div id="pointsText" style="display:none;">
  <p style="font-size: 18pt;">
    <span style="font-size: 22pt;" id="name"></span>,<br>
    You have <strong style="font-size: 20pt;" id="points"></strong>.<br>
    You need <strong id="max" style="font-size: 20pt;"></strong> points to be inducted.<br>
    <progress style="width:100%" id="prog" value="0" max="0"></progress><br>
    <span style="font-size: 15pt;">You are <strong style="font-size:20pt;" id="percent"></strong> of the way there! Keep up the good work.</span>
  </p>
</div>
<div id="instructions">Enter your name below to access points.
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">google.load("visualization", "1");</script>
<script type="text/javascript" src="points.js"></script>

<form id="pointsForm" class="pure-form" action="javascript:loadPoints();">
  <input type="text" id="pin" placeholder="Full Name" autofocus>
  <button type="submit" class="pure-button pure-button-primary" id="loadPoints">Get Points</button>
  <div id="error" style="color:red;"></div>
</form>
</div>
<br>
<p>Check the brand new <a href="/leaderboard">Points Leaderboard</a> to see who you're up against and beat your friends!</p>
<span style="font-size: 15pt;">
Come to meetings for 1 point, and events for 2.<br>
There will also be various opportunities to make up points throughout the year.<br>
If you're in need of points, [contact us](/contact) and we might be able to arrange something.
</span>
