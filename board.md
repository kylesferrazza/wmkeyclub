---
layout: page
title: Board Members
weight: 2
---
Click any name to send an email.

Our board members for the 2016-2017 service year are:

* President [Amy Huang](mailto:president@wmkeyclub.com)
* Vice Presidents [Erin Pitcher](mailto:vp@wmkeyclub.com), [Joaquin Cortes](mailto:vp@wmkeyclub.com) and [Megan Specht](mailto:@wmkeyclub.com)
* Treasurer [Morgan Persky](mailto:treasurer@wmkeyclub.com)
* Secretaries [Michael Selvaggio](mailto:secretary@wmkeyclub.com) and [Anna Skolnick](mailto:secretary@wmkeyclub.com)
* 9th Grade Representative [Sareen Nezaria](mailto:9thgraderep@wmkeyclub.com)
* Lieutenant Governor Natalie D'Onofrio
* Editor [Niki Nassiri](mailto:editor@wmkeyclub.com)
* Webmaster [Kyle Sferrazza](mailto:webmaster@wmkeyclub.com)


Our board members for the 2017-2018 service year are:
* President Niki Nassiri
* Vice Presidents Megan Specht, Ally Szema, and Julia Yabut 
* Treasurer Hannah Fondacaro
* Secretaries Britini Guthy and Mikayla O'Leary
* 9th Grade Representatives Sareen Nezaria (Murphy) and Diana Yu (Gelinas)
* Lieutenant Governor Morgan Persky
* Editor Leah Sugrue
* Webmaster Haripriya Bhuttaru
