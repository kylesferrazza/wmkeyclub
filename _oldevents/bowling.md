---
layout: event
title: Bowling Fundraiser for Boys & Girls Club
date: 2016-11-06
times: 2pm - 4pm
---
- Come for a fun bowling day at Port Jefferson Lanes.
- Includes 2 hours of unlimited bowling, shoe rental, pizza & soda, and cupcakes.
- Buffalo Wild Wings will be doing a **WING DROP**!
- It's $20 if you pay by Friday, November 4th or $25 at the door.
- If you want to pay in advance, bring the money to Mrs. Goldberg in Room 108 (across from the library) during the school day.
