---
layout: event
title: 23rd Annual Walk-Along for Lupus
date: 2016-10-16
times: 8:30am
signup: http://lupusliqueens.org/events/23walk/
msg: Register Online
---
<ul>
  <li>Every donation to the Walk-Along for Lupus directly helps someone affected by the disease in Nassau, Suffolk, and Queens.</li>
  <li>The organization Lupus Alliance of Long Island/Queens is proud to have raised over $200,000 every year.</li>
  <li>Walk-Along for Lupus Donations Fund; Support groups, Kids Group, Telephone Counseling, Symposiums, Scholarships, Financial Grants, Online Programs</li>
  <li><strong>If you register for this event, please send a screenshot of your registration confirmation to <a href="mailto:board@wmkeyclub.com">board@wmkeyclub.com</a> in order for us to verify that you signed up for this event and attended it</strong></li>
</ul>
