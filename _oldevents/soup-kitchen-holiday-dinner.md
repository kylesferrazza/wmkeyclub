---
layout: event
title: Soup Kitchen Holiday Dinner
date: 2016-12-18
times: 11:30-2 or 2-4:30
msg: Sign up here!
signup: https://docs.google.com/forms/d/e/1FAIpQLSesaV4mjtsWXt53nY3z2oJTl3_qOY75xU3hGbX38ojTu1JC1A/viewform
---
<ul>
  <li>Volunteer at St. James Soup Kitchen and help serve the holiday meals!</li>
  <li>Address: St. James Roman Catholic Church, 429 NY-25A</li>
  <li>Extra hands are needed on this busy day to give out gifts and serve dinner!</li>
  <li>Two shifts:
    <ul>
      <li>11:30 - 2</li>
      <li>2 - 4:30</li>
    </ul>
  </li>
</ul>
