---
layout: event
title: Annual Long Island Apple Festival
date: 2016-09-25
times: See below for shift times and information
---
- Support this local event at the Sherwood Jayne House and get in the spirit of fall!
- The only way to sign up for this event is by signing the sheet at the key club meeting on September 21st
- Volunteer assignments include
  - distributing tickets
  - selling food
  - arts & crafts
  - selling apples/popcorn
  - working Benner’s Farm
  - tattoo art
- Shifts are:
  - 10:30am-2pm
  - 1:30pm-5:30pm
  - FULL DAY shift option is 10:30am-5:30pm [only Full day volunteers will receive a lunch voucher]
- Next Wednesday, Sept 21st, you will be assigned a job and shift time.
- Everyone needs to wear a hat and hair in ponytails.
