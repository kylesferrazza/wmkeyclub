---
layout: event
title: Stony Brook Train Station Cleanup
date: 2016-10-08
times: 8am-11am
signup: https://docs.google.com/forms/d/e/1FAIpQLSf0QH7dI-2_q4DZgZXcuc03UhXxbN7tTnpdDityKZgdiALYKw/viewform
msg: Sign up here!
---
<ul>
  <li>Help keep our local train station clean at this annual fall community clean up at Stony Brook Train Station</li>
  <li>This event is on, rain or shine</li>
  <li>Activities include trimming bushes, picking up trash, raking leaves, sweeping salt, sand, and dirt, spreading mulch, and planting flowers</li>
  <li>Bagels and Coffee will be provided by Bagel Express</li>
</ul>
