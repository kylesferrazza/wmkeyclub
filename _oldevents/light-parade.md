---
layout: event
title: "Electric Light Parade"
date: 2016-12-11
signup: https://docs.google.com/forms/d/e/1FAIpQLSeyie9QS4915oPNnvM1-FIfQReGxjCM0cgngOQokioI9VUCQQ/viewform
msg: Sign up here for either volunteer option
---
<ul>
  <li>The parade is on Sunday, December 11th.</li>
  <li>There are two volunteer options.</li>
  <li>People are needed to hand out cookies and hot chocolate by Seport Deli - wear key club shirts, get there at 4:30pm.</li>
  <li>OR</li>
  <li>If you sign up to march in the parade, arrive by 4pm to Setauket School.</li>
</ul>
