---
layout: event
title: Support the Kids 5K
date: 2016-06-11
times: 10:30am-12:30pm
moreinfo: http://supportthekid.org/event/3rd-annual-5k-for-the-kids/
signup: https://www.eventbrite.com/e/3rd-annual-5k-for-the-kids-tickets-23190597702
msg: "Register Here"
---
Come support this great organization that gives back to families of child cancer patients. Registration begins at 9:30am or online.
Once you've signed up for the race, be sure to fill out <a href="http://goo.gl/forms/vPRLBO2PoA2XrPl83">this form</a> to receive points.
