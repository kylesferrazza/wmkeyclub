---
layout: event
title: Making Strides Against Breast Cancer Walk
date: 2016-10-16
times: and October 23rd, 2016
moreinfo: /events/making-strides-walk/poster.pdf
---
- 5 Mile Non-Competitive Fundraising Walk (3 Points) 
  - **Date**: October 16th, 8am-11am
  - **Location**: Jones Beach State Park, Parking Field 5
- 5K Non-Competitive Fundraising Walk (3 Points)
  - **Date**: October 23rd- Registration: 7:30am, Start: 8:30am
  - **Location**: Suffolk County Community College, Eastern Campus- Riverhead
- As the second-leading cause of cancer death in women, 40, 450 women in the US are expected to die from breast cancer this year. While the statistics may be staggering, we have an advantage over cancer: the power of humanity. Join today and lend your passion and courage to the cause. Join us to celebrate cancer survivors, pay tribute to loved ones lost, and raise funds to help us free the world from the pain and suffering of cancer. The American Cancer Society Making Strides Against Breast Cancer Walk is a powerful event to raise awareness and funds to end breast cancer. 
- These two events are **Fundraising Alternatives**, if you are unable to do the Yankee Candle Fundraiser.
- If you attend either of these events, you will not only receive fundraiser credit (Members must complete 1 of 2 Fundraisers in order to be inducted), but you will also receive 3 points for event credit
- If you register for either walk events, please send a screenshot of your registration confirmation to board@wmkeyclub.com in order for us to verify that you signed up for the event and attended it
- [Sign up for 5 Mile Fundraising Walk here](http://www.makingstrideswalk.org/longisland){:target="_blank"}
- [Sign up for 5K Fundraising Walk here](http://www.makingstrideswalk.org/easternlongisland){:target="_blank"}
