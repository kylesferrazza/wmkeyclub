---
layout: event
title: Fall Rally
date: 2016-10-23
---
- October 23rd
- Location: LIU Post
- Reach out to LTG Natalie D’Onofrio for more details
  - [nataliedonofrio.ltg@nydkc.org](mailto:nataliedonofrio.ltg@nydkc.org)
