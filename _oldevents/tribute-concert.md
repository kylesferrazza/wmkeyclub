---
layout: event
title: Kiwanis Tribute Concert
date: 2017-03-18
times: 7:30pm
msg: Sign up to help out here
signup: https://docs.google.com/forms/d/e/1FAIpQLSdLJseSYQWFiNCMtsyl4cUScyH4ws3ReH9KlWIPagAmw-9OKA/viewform?usp=sf_link
---
- Kiwanis needs our help for their tribute concert on March 18th!
- March 18th
- 7:30pm
- [Poster with more info](http://wmkeyclub.com/public/poster.png) 
