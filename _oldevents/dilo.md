---
layout: "event"
title: "DiLo Guild Awards"
date: 2016-06-02
signup: http://goo.gl/forms/gqBpxYW4vq
---
Volunteers are needed to help at this spectacular event to sell tickets, stand over raffle baskets, hand out programs, etc.
Great entertainment, great service, great night. Sign up!