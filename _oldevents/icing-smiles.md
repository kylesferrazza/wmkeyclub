---
layout: event
title: Icing Smiles Fundraiser
date: 2017-01-12
times: through February 1
msg: Sign up here!
signup: https://docs.google.com/forms/d/e/1FAIpQLSdCDjiunUO17uWPwZraG7HCW8iUcsYUftN9KPdVFRjddomG5g/viewform
---
- Key Club needs volunteers to promote and collect money for Icing Smiles, a nonprofit organization that provides celebration cakes to families impacted by the critical illness of a child. Volunteer to be in charge of our table during free periods.

- January 12th through February 1st, lunch periods (4 - 8), in the commons.
- 4 POINTS per period.

- Fill out the form multiple times if you would like multiple dates.
- Multiple periods on the same day can be accomplished on one submission of the form.
