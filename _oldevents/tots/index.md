---
layout: event
title: Trick-or-Treat Street
date: 2016-10-28
times: 4pm-7pm
signup: https://docs.google.com/forms/d/e/1FAIpQLSeFFjTmhLrLlTlTujxkg3tgZW2P32NCeD0COTrhhAbdZWWx4g/viewform
msg: Sign up here
---
- Location: Ward Melville High School
- Trick-or-Treat Street is an annual school-wide event at Ward Melville where nearly all the WM clubs come together to create a fun, safe trick-or-treating environment for kids. Key Club is responsible for organizing and conducting the entire event. All help is needed!
- TOTS is one of three **mandatory events** for Key Club. All Key Clubbers must volunteer for at least 2 hours at TOTS in order to receive mandatory event credit. 
- The following committees are: Donations, Set-up, Candy, Decorations, Rooms, Clean-up
- If you are not part of a committee already, please sign up for one ASAP. 
- Donations: All members are a part of Donations Committee. Please donate at least $5. 
- Decorations/Set-up: 2pm-4pm
- Candy: 2:30pm-5pm OR 5pm-7:30pm
- Rooms: 4pm-6pm OR 6pm-8pm
- Clean-up: 7pm-9pm
- [Click here for commitee info](/events/tots/committee-info.pdf){:target="blank"}
