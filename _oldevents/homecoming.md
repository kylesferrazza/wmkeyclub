---
layout: event
title: "Homecoming Carnival"
date: 2016-09-24
times: 11:30am-1:15pm
signup: https://docs.google.com/forms/d/e/1FAIpQLSdYMcJOdVfIkhq1Vh6nRDr-BWGnN9hHzBPI0vupAJBa1C7Hsw/viewform
msg: Sign up here.
---
- Come support Ward Melville and bring your SCHOOL SPIRIT! We will have a booth for Key Club!
- Wear green and gold
- What Key Club's booth will be doing is yet to be determined.
