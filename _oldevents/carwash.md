---
layout: "event"
title: "Car Wash"
date: 2016-06-18
signup: http://goo.gl/forms/jGrD9Fn4ikpuFHW93
msg: "Sign Up Here"
---
Good June weather calls for a car wash! Volunteers should bring a rag or towel to dry cars.

Shift 1: 10am - 12:30pm

Shift 2: 11:30am - 2pm