---
layout: event
title: Making Strides Against Breast Cancer Fundraiser
date: 2016-10-17
times: through the 21st
signup: https://docs.google.com/forms/d/e/1FAIpQLSfoIZiTHe8_MvXSFRkRs--jnUvgl0kR5XCZVhnr_c-CxfKkCg/viewform
msg: Signup / more info!
---
- Key Club needs students to come down to the Commons during free periods to be in charge of our coin fundraising table. 
- Students can only come during empty/free periods. Students are NOT permitted to miss class for this event; they are responsible for their actions. 
- We are asking students to fill water bottles up with dimes in order to raise $100 for the American Cancer Society
- We will have empty water bottles for dimes and 1 bottle for other coins.
- <a target="_blank" href="/events/making-strides-fundraiser/poster.pdf">Informational poster</a>
