---
layout: event
title: Key Club International Convention (ICONN)
date: 2016-07-06
moreinfo: http://keyclub.org/events/convention.aspx
---
Every summer Key Club International celebrates a year of service at the international convention. Over 2,000 students and advisors from all over the world gather together to introduce new programs, present awards to outstanding clubs, conduct the organization's business, attend educational workshops and elect international officers for the upcoming year.

The 73rd Annual Key Club International Convention will take place in Atlanta, Georgia July 6 - 10, 2016.
