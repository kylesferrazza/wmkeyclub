---
layout: event
title: NYS K-12 Kids Safe Online Poster Contest
date: 2016-11-09
---
<ul>
  <li>Nov. 9th Deadline</li>
  <li>10 points!</li>
  <li>Make a poster for National Cyber Security Awareness Month</li>
  <li>Five best posters will be selected for contest entry</li>
  <li>Posters must adhere to rules and guidelines, posters must be appropriate</li>
  <li><a target="_blank" href="/events/safe-online/postercontest.pdf">Click here for poster guidelines.</a></li>
</ul>

<form action="https://getsimpleform.com/messages?form_api_token=166878430893e11f56f15d389f77f53b" enctype="multipart/form-data" method="post">
  <input type='hidden' name='redirect_to' value='http://wmkeyclub.com/contact/thanks' />
  Name: <input type='text' name='Name' required /><br>
  Grade:
  <select>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
  </select><br>
  Phone Number: <input type='text' name='Phone Number' required /><br>
  <input type='file' name='poster' accept="image/*" required /><br>
  <input type='submit' value='Submit' />
</form>
