---
layout: "event"
title: "Tropical Smoothie Day"
date: 2016-06-08
signup: http://goo.gl/forms/1BtX4T6HEO2SebeT2
---
Money is due June 1st! There are NO EXCEPTIONS.

<a href="https://www.tropicalsmoothie.com/fresh-smoothies" target="_blank">Click here</a> for the menu.

Be sure to check all of the tabs ("Fresh Smoothies", "Indulgent Smoothies") to see all of the options.