---
layout: event
title: Flapjack Fundraiser for the Brookhaven Youth Bureau
date: 2016-11-05
times: 7am-10am
signup: https://docs.google.com/forms/d/e/1FAIpQLSe7i_vnYk7u_HGQFJUpq4YWg810cx3rU9AQ16gD9FMsHs1iMA/viewform#responses
msg: Sign-up here!
---
Kiwanis asks for help from WM Key Club volunteers to serve breakfast.
(Applebees, North Ocean Avenue, Farmingville)
