---
layout: event
title: Mission Toothbrush
date: 2017-04-29
times: 10am-5pm, multiple shifts
msg: Signup here!
signup: https://docs.google.com/forms/d/e/1FAIpQLSc_pzR7I7SMDvgXr8G5l8EMpTsv7-1AqI3egiqhWialPFbxyA/viewform?usp=sf_link
---
- Key Club is partnering with Mission Toothbrush for this awesome initiative. Come to Stop & Shop and encourage local shoppers to buy/donate a toothbrush.
- **WHERE:** [Stop & Shop on 25A in Setauket][stopshop]
- **WHEN:** Saturday, April 29th
- 3 POINTS PER SHIFT (one hour)!

[stopshop]: https://www.google.com/maps/place/Stop+%26+Shop/@40.9374573,-73.1080507,17.04z/data=!4m8!1m2!2m1!1sstop+and+shop+setauket!3m4!1s0x89e83f74efbcb591:0xe1be28d243f29d0!8m2!3d40.93935!4d-73.108771
