---
layout: event
title: Taste of The Neighborhood
date: 2017-05-12
times: multiple shifts
msg: Signup here!
signup: https://docs.google.com/forms/d/e/1FAIpQLSdkS92ZXKSEUf3u59h3bFwBrwxFJPW7fgm9k9jBHFJmxqQVng/viewform?usp=sf_link
---
- Friday May 12th
- 2 shifts
  - 6:15-8:10
  - 8:30-10pm
