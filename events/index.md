---
layout: page
title: "Events - Sign Up"
weight: 3
---
All events are currently worth 2 points, *unless otherwise noted*.
<hr>
<div class="posts">
  {% assign pages_list = site.pages | sort:"date" %}
  {% for post in pages_list %}
  {% if post.layout == "event" %}
  <div class="post" style="margin-bottom:0px;">
    <h2 class="post-title">{{post.title}}</h2>
    <span class="post-date">{{ post.date | date: '%B %d, %Y' }}{% if post.times %}, {{post.times}}{% endif %}
      {% if post.signup %}<br><a href="{{post.signup}}" target="_blank">{{post.msg}}</a>{% endif %}
      {% if post.moreinfo %}<br><a href="{{post.moreinfo}}" target="_blank">More Information</a>{% endif %}
    </span>
    {{ post.content | markdownify }}
    <hr>
  </div>
  {% endif %}
  {% endfor %}
  <div class="post" style="margin-bottom:0px;">
    <h2 class="post-title">Boys and Girls Club</h2>
    <span class="post-date">
      <br><a href="https://docs.google.com/forms/d/e/1FAIpQLSfmW8ww0AVpvqxIDn6_nQw1mwG0pOvTKSc20lThAVtsuY5AwA/viewform" target="_blank">Sign-up here!</a>
    </span>
    <ul>
      <li>The Boys and Girls Club needs your help! Sign up to come on any weekday to help students from Kindergarten through middle school with various things such as homework or keeping them entertained.</li>
      <li>324 Jayne Boulevard, Port Jeff Station</li>
    </ul>
    <hr>
  </div>
</div>
